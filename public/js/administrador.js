function limpiarFormCompany(){
    $('#namec').val("");
    $('#correoc').val("");
    $('#logoc').val("");
    $('.alert-danger').hide("fast");
    $('.alert-danger').html("");
}function limpiarFormEmployee(){
    $.each($('input'),function(index,valor){
        $(this).val("");
    });
    $('#companyid').val(0);
    $('.alert-danger').hide("fast");
    $('.alert-danger').html("");
}
function limpiarFormCompanyU(){
    $('#nameca').val("");
    $('#correoca').val("");
    $('#logoca').val("");
    $('.alert-danger').hide("fast");
    $('.alert-danger').html("");
}
function delCompany(idc){
    $('#deleteCompany').data('target',idc);
    $('#confirm').modal({
        backdrop: 'static',
        keyboard: false
    });
}
function delEmployee(ide){
    $('#deleteEmployee').data('target',ide);
    $('#confirm').modal({
        backdrop: 'static',
        keyboard: false
    });
}




$(function () {
    $('#deleteCompany').on('click',function (e) {
        var idc=$(this).data('target');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            url: public_url + 'company/'+idc,
            data: '',
            success: function (datos) {
                if (datos.success) {
                    alert('Compañía borrada correctamente.');
                    location.reload(true);
                }else{
                    alert('Ha ocurrido un problema al intentar de ejecutar su solicitud.');
                    location.reload(true);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                location.reload(true);
            }
        });
    });
    $('#deleteEmployee').on('click',function (e) {
        var ide=$(this).data('target');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            url: public_url + 'employee/'+ide,
            data: '',
            success: function (datos) {
                if (datos.success) {
                    alert('Empleado borrado correctamente.');
                    location.reload(true);
                }else{
                    alert('Ha ocurrido un problema al intentar de ejecutar su solicitud.');
                    location.reload(true);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                location.reload(true);
            }
        });
    });
    $('#createCompany').on('click',function(e){
        e.preventDefault();
        var cc = new FormData();
        var nombre=$('#namec').val(), correo=$('#correoc').val(),logo = $('#logoc')[0].files[0];
        cc.append('nombre',nombre);
        cc.append('correo',correo);
        cc.append('logo',logo);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            url:public_url+'company',
            data:cc,
            contentType: false,
            processData: false,
            success:function(datos){
                $('.alert-danger').hide("fast");
                $('.alert-danger').html("");
                $('.alert-success').hide("fast");
                if(!datos.success) {
                    $.each(datos.errors, function (key, value) {
                        $('.alert-danger').show("fast");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }else{
                    $('.alert-success').show("fast");
                    limpiarFormCompany();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                location.reload(true);
            }
        });
    });
    $('#createEmployee').on('click',function(e){
        e.preventDefault();
        var formData = new FormData();
        var nombres=$('#namese').val(),apellidos=$('#lastnamese').val(),cid=$('#companyid').val(), correo=$('#correoe').val(), telefono=$('#phonee').val();
        if(cid==0){
            alert('Seleccione una compañía.');
            return;
        }
        formData.append('nombres',nombres);
        formData.append('apellidos',apellidos);
        formData.append('cid',cid);
        formData.append('correo',correo);
        formData.append('telefono',telefono);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            url:public_url+'employee',
            data:formData,
            contentType: false,
            processData: false,
            success:function(datos){
                $('.alert-danger').hide("fast");
                $('.alert-danger').html("");
                $('.alert-success').hide("fast");
                if(!datos.success) {
                    $.each(datos.errors, function (key, value) {
                        $('.alert-danger').show("fast");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }else{
                    $('.alert-success').show("fast");
                    limpiarFormEmployee();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                location.reload(true);
            }
        });
    });

    $('#updateCompany').on('click',function(e){
        e.preventDefault();
        var formData = new FormData();
        var nombrea=$('#nameca').val(), correoa=$('#correoca').val(),logoa= $('#logoca')[0].files[0], idca=$(this).data('target');

        formData.append('_method','PUT');
        formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
        formData.append('nombre',nombrea);
        formData.append('correo',correoa);
        formData.append('logo',logoa);
        $.ajax({
            url:public_url+'company/'+idca,
            cache: false,
            type:'POST',
            dataType:'json',
            data:formData,
            contentType: false,
            processData: false,
            success:function(datos){
                $('.alert-danger').hide("fast");
                $('.alert-danger').html("");
                $('.alert-success').hide("fast");
                if(!datos.success) {
                    $.each(datos.errors, function (key, value) {
                        $('.alert-danger').show("fast");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }else{
                    alert('La compañía ha sido actualizada correctamente.');
                    location.reload(true);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //location.reload(true);
            }
        });
    });

    $('#updateEmployee').on('click',function(e){
        e.preventDefault();
        var formData = new FormData();
        var nombres=$('#nameseu').val(),apellidos=$('#lastnameseu').val(),cid=$('#companyidu').val(), correo=$('#correoeu').val(), telefono=$('#phoneeu').val(), idea=$(this).data('target');
        if(cid==0){
            alert('Seleccione una compañía.');
            return;
        }

        formData.append('_method','PUT');
        formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
        formData.append('nombres',nombres);
        formData.append('apellidos',apellidos);
        formData.append('cid',cid);
        formData.append('correo',correo);
        formData.append('telefono',telefono);
        $.ajax({
            url:public_url+'employee/'+idea,
            cache: false,
            type:'POST',
            dataType:'json',
            data:formData,
            contentType: false,
            processData: false,
            success:function(datos){
                $('.alert-danger').hide("fast");
                $('.alert-danger').html("");
                $('.alert-success').hide("fast");
                if(!datos.success) {
                    $.each(datos.errors, function (key, value) {
                        $('.alert-danger').show("fast");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }else{
                    alert('El empleado ha sido actualizado correctamente.');
                    location.reload(true);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //location.reload(true);
            }
        });
    });
})

