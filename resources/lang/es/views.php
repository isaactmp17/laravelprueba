<?php
return [
    // LISTADO DE COMPAÑIAS

    // Titulos
    'company-list-t1' => 'Listado de compañías (Paginador Laravel)',
    'company-list-t2' => 'Listado de compañías (DataTables)',

    // Tabla 1
    'company-t1-name' => 'Nombre',
    'company-t1-email' => 'Correo',
    'company-t1-actions' => 'Acciones',
    'company-t1-bt1' => 'Eliminar',
    'company-t1-bt2' => 'Ver',

    // Tabla 2
    'company-t2-name' => 'Nombre',
    'company-t2-email' => 'Correo',

    'companies-notfound' => 'Actualmente, no hay compañías creadas.',
    'company-confirmd' => ['¿Estás seguro?','Si le das al botón continuar la compañía y sus empleados serán borrados.','Continuar','Cancelar']

    // FIN LISTADO DE COMPAÑIAS
];
