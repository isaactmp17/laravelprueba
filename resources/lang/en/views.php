<?php
return [
    // LISTADO DE COMPAÑIAS

    // Titulos
    'company-list-t1' => 'Laravel Paginator',
    'company-list-t2' => 'Datatables',

    // Tabla 1
    'company-t1-name' => 'Name',
    'company-t1-email' => 'Email',
    'company-t1-actions' => 'Actions',
    'company-t1-bt1' => 'Delete',
    'company-t1-bt2' => 'Show',

    // Tabla 2
    'company-t2-name' => 'Name',
    'company-t2-email' => 'Email',

    'companies-notfound' => 'There are currently no companies created.',
    'company-confirmd' => ['Are you sure?','If you click on the button continue the company and its employees will be deleted.','Continue','Cancel']
    // FIN LISTADO DE COMPAÑIAS
];
