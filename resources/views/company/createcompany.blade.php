@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crea una compañía</div>
                <div class="card-body">
					<div class="alert alert-danger" style="display:none"></div>
					<div class="alert alert-success" style="display:none">La compañía ha sido creada exitosamente.</div>
                    <div class="form-group">
						<label for="namec">Razón Social / Nombre de la compañía</label>
						<input type="text" class="form-control" id="namec" max="191">
					</div>
                    <div class="form-group">
						<label for="correoc">Correo</label>
						<input type="email" class="form-control" id="correoc" max="191">
					</div>
					<div class="form-group">
						<label for="logoc">Logo de la compañía</label>
						<input type="file" class="form-control-file" id="logoc">
					</div>
                </div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" id="createCompany">Crear</button>
					<button type="button" class="btn btn-secondary" onClick="limpiarFormCompany()">Limpiar Formulario</button>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
@endsection