@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center font-weight-bold">{{$company->name}}</div>

                <div class="card-body text-center">
                    <img src="{{asset('storage/'.$company->id.'.png?v='.rand())}}" class="img-fluid">
					<hr>
					<h4 class="font-weight-bold">Detalles</h4>
					<hr>
					<strong>Correo: </strong> <br>
					{{$company->email}}
					<br><br>
					<strong>Cantidad de empleados: </strong><br>{{count($company->employee)}}
                </div>
				<div class="card-footer text-center">
					<a class="btn btn-secondary " href="{{asset('company')}}">Volver al listado</a>
					<a class="btn btn-warning " href="{{asset('company/'.$company->id.'/edit')}}">Modificar</a>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
