@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center font-weight-bold">{{$company->name}}</div>

                <div class="card-body text-center">
                    <img src="{{asset('storage/'.$company->id.'.png?v='.rand())}}" class="img-fluid">
					<hr>
					<h4 class="font-weight-bold">Detalles</h4>
					<hr>
					<strong>Correo: </strong> <br>
					{{$company->email}}
                </div>
				<div class="card-footer text-center">
					<a class="btn btn-secondary " href="{{asset('company')}}">Volver al listado</a>
				</div>
            </div>
        </div>
		<div class="col-md-8">
			<div class="card">
                <div class="card-header bg-info text-white">Actualizar compañía</div>
                <div class="card-body">
					@csrf
					<div class="alert alert-danger" style="display:none"></div>
                    <div class="form-group">
						<label for="nameca">Razón Social / Nombre de la compañía</label>
						<input type="text" class="form-control" id="nameca" max="191" value="{{$company->name}}">
					</div>
                    <div class="form-group">
						<label for="correoca">Correo</label>
						<input type="email" class="form-control" id="correoca" max="191" value="{{$company->email}}">
					</div>
					<div class="form-group">
						<label for="logoca">Logo de la compañía</label>
						<input type="file" class="form-control-file" id="logoca" aria-describedby="lHelp">
						<small id="lHelp" class="form-text text-muted">Si deja este campo vacío, no se actualizará el logo.</small>
					</div>
                </div>
				<div class="card-footer">
					<button type="button" class="btn btn-success" id="updateCompany" data-target="{{$company->id}}">Actualizar</button>
					<button type="button" class="btn btn-secondary" onClick="limpiarFormCompanyU()">Limpiar Formulario</button>
				</div>
            </div>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
@endsection