@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{!! trans('views.company-list-t1') !!} </div>

                <div class="card-body">
					@if(count($companies)>0)
					<table class="table table-hover">
					  <thead>
						<tr class="bg-primary text-white">
						  <th scope="col">#</th>
						  <th scope="col">Logo</th>
						  <th scope="col">{!! trans('views.company-t1-name') !!}</th>
						  <th scope="col">{!! trans('views.company-t1-email') !!}</th>
						  <th scope="col">{!! trans('views.company-t1-actions') !!}</th>
						</tr>
					  </thead>
					  <tbody>
						@foreach ($companies as $company)
						<tr>
						  <th scope="row">{{ $company->id }}</th>
						  <td><img src="{{ asset('storage/'.$company->id.'.png?v='.rand()) }}" width="150px"></td>
						  <td>{{ $company->name }}</td>
						  <td>{{ $company->email }}</td>
						  <td>
							 <button class="btn btn-danger" onClick="delCompany({{$company->id}})">{!! trans('views.company-t1-bt1') !!}</button>
							  <a class="btn btn-primary text-white" href="{{asset('company/'.$company->id)}}">{!! trans('views.company-t1-bt2') !!}</a>
						  </td>
						</tr>
						@endforeach
					  </tbody>
					</table>
					
					<div class="modal" id="confirm" tabindex="-1" role="dialog">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header bg-danger text-white">
							<h5 class="modal-title">{!! trans('views.company-confirmd')[0] !!}</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<p>{!! trans('views.company-confirmd')[1] !!}</p>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('views.company-confirmd')[3] !!}</button>
							<button type="button" class="btn btn-warning" data-dismiss="modal" id="deleteCompany">{!! trans('views.company-confirmd')[2] !!}</button>
						  </div>
						</div>
					  </div>
					</div>
					@else
						{!! trans('views.companies-notfound') !!}
					@endif
                </div>
				<div class="card-footer">
					{{ $companies->links() }}
				</div>
            </div>
        </div>
    </div>
	<div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{!! trans('views.company-list-t2') !!}</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="dataTablesCompany">
						  <thead>
							<tr class="bg-primary text-white">
							  <th scope="col">#</th>
							  <th scope="col">{!! trans('views.company-t2-name') !!}</th>
							  <th scope="col">{!! trans('views.company-t1-name') !!}</th>
							</tr>
						  </thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
<script>
$(function () {
    $('#dataTablesCompany').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			url:"{{asset('company')}}"
		},
		columns:[
			{ data: 'id', name: 'id' },
        	{ data: 'name', name: 'name' },
        	{ data: 'email', name: 'email' },
		]
	});
});
</script>
@endsection
