<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <ul class="navbar-nav ml-auto">
						
						@auth
							<li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">{!!trans('menu.home')!!}</a>
                            </li>
						
							<li class="nav-item dropdown">
                                <a id="navbarDropdownC" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {!!trans('menu.company')!!} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownC">
                                    <a class="dropdown-item" href="{{ asset('company') }}">
                                       	{!!trans('menu.company-list')!!}
                                    </a>
									<a class="dropdown-item" href="{{ asset('company/create') }}">
                                       	{!!trans('menu.company-create')!!}
                                    </a>
                                </div>
                            </li>
							<li class="nav-item dropdown">
                                <a id="navbarDropdownC" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{trans('menu.employee')}} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownC">
                                    <a class="dropdown-item" href="{{ asset('employee') }}">
                                       	{!!trans('menu.employee-list')!!}
                                    </a>
									<a class="dropdown-item" href="{{ asset('employee/create') }}">
                                       	{!!trans('menu.employee-create')!!}
                                    </a>
                                </div>
                            </li>
						@endauth
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {!!trans('menu.logout')!!}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
						<li class="nav-item dropdown">
							<a id="navbarDropdownC" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								<i class="fas fa-globe-americas"></i> <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownC">
								
								@if (config('locale.status') && count(config('locale.languages')) > 1)
										@foreach (array_keys(config('locale.languages')) as $lang)
											@if ($lang != App::getLocale())
												<a class="dropdown-item" href="{!! route('lang.swap', $lang) !!}">
														@if($lang=='en')
														English
														@else
														Español
														@endif
												</a>
											@endif
										@endforeach
								@endif
							</div>
						</li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script>
		var public_url="{{asset('/')}}";
	</script>
	@yield('scripts')
</body>
</html>
