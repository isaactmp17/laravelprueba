@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center font-weight-bold">{{$employee->names.' '.$employee->lastnames}}</div>

                <div class="card-body text-center">
					<h4 class="font-weight-bold">Detalles</h4>
					<hr>
					<strong>Compañía a la que pertenece: </strong> <br>
					{{$employee->company->name}}
					<br><br>
					<strong>Correo: </strong> <br>
					{{$employee->email}}
					<br><br>
					<strong>Teléfono: </strong> <br>
					{{$employee->phone}}
					<br><br>
                </div>
				<div class="card-footer text-center">
					<a class="btn btn-secondary " href="{{asset('employee')}}">Volver al listado</a>
					<a class="btn btn-warning " href="{{asset('employee/'.$employee->id.'/edit')}}">Modificar</a>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
