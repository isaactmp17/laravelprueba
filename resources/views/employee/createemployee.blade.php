@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crea un empleado</div>
                <div class="card-body">
					@if(count($companies)>0)
					<div class="alert alert-danger" style="display:none"></div>
					<div class="alert alert-success" style="display:none">El empleado ha sido creado exitosamente.</div>
                    <div class="form-group">
						<label for="namese">Nombres</label>
						<input type="text" class="form-control" id="namese" max="191">
					</div>
                    <div class="form-group">
						<label for="lastnamese">Apellidos</label>
						<input type="text" class="form-control" id="lastnamese" max="191">
					</div>
					<div class="form-group">
						<label for="companyid">Compañía a la que pertenece</label>
						<select class="form-control" id="companyid">
							<option value="0" selected>Seleccione...</option>
							@foreach($companies as $company)
							<option value="{{$company->id}}">{{$company->name}}</option>
							@endforeach
						</select>
					</div>
                    <div class="form-group">
						<label for="correoe">Correo</label>
						<input type="email" class="form-control" id="correoe" max="191">
					</div>
                    <div class="form-group">
						<label for="phonee">Teléfono</label>
						<input type="tel" class="form-control" id="phonee" max="191">
					</div>
					@else
					Debe crear al menos una compañía para poder crear un empleado.					
					@endif
                </div>
				<div class="card-footer">
					@if(count($companies)>0)
					<button type="button" class="btn btn-primary" id="createEmployee">Crear</button>
					<button type="button" class="btn btn-secondary" onClick="limpiarFormEmployee()">Limpiar Formulario</button>
					@endif
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
@endsection