@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center font-weight-bold">{{$employee->names.' '.$employee->lastnames}}</div>

                <div class="card-body text-center">
					<h4 class="font-weight-bold">Detalles</h4>
					<hr>
					<strong>Compañía a la que pertenece: </strong> <br>
					{{$employee->company->name}}
					<br><br>
					<strong>Correo: </strong> <br>
					{{$employee->email}}
					<br><br>
					<strong>Teléfono: </strong> <br>
					{{$employee->phone}}
					<br><br>
                </div>
				<div class="card-footer text-center">
					<a class="btn btn-secondary " href="{{asset('employee')}}">Volver al listado</a>
				</div>
            </div>
        </div>
		<div class="col-md-8">
			<div class="card">
                <div class="card-header bg-info text-white">Actualizar empleado</div>
                <div class="card-body">
					<div class="alert alert-danger" style="display:none"></div>
					<div class="form-group">
						<label for="namese">Nombres</label>
						<input type="text" class="form-control" id="nameseu" max="191" value="{{$employee->names}}">
					</div>
                    <div class="form-group">
						<label for="lastnamese">Apellidos</label>
						<input type="text" class="form-control" id="lastnameseu" max="191" value="{{$employee->lastnames}}">
					</div>
					<div class="form-group">
						<label for="companyid">Compañía a la que pertenece</label>
						<select class="form-control" id="companyidu">
							@foreach($companies as $company)
							<option value="{{$company->id}}" @if($company->id==$employee->company_id) selected @endif>{{$company->name}}</option>
							@endforeach
						</select>
					</div>
                    <div class="form-group">
						<label for="correoe">Correo</label>
						<input type="email" class="form-control" id="correoeu" max="191" value="{{$employee->email}}">
					</div>
                    <div class="form-group">
						<label for="phonee">Teléfono</label>
						<input type="tel" class="form-control" id="phoneeu" max="191" value="{{$employee->phone}}">
					</div>
                </div>
				<div class="card-footer">
					<button type="button" class="btn btn-success" id="updateEmployee" data-target="{{$employee->id}}">Actualizar</button>
				</div>
            </div>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
@endsection