@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Listado de empleados (Paginador Laravel)</div>

                <div class="card-body">
					@if(count($employees)>0)
					<table class="table table-hover">
					  <thead>
						<tr class="bg-primary text-white">
						  <th scope="col">#</th>
						  <th scope="col">Nombres</th>
						  <th scope="col">Apellidos</th>
						  <th scope="col">Compañía</th>
						  <th scope="col">Correo</th>
						  <th scope="col">Teléfono</th>
						  <th scope="col">Acciones</th>
						</tr>
					  </thead>
					  <tbody>
						@foreach ($employees as $employee)
						<tr>
						  <th scope="row">{{$employee->id}}</th>
						  <td>{{$employee->names}}</td>
						  <td>{{$employee->lastnames}}</td>
						  <td>{{$employee->company->name}}</td>
						  <td>{{$employee->email}}</td>
						  <td>{{$employee->phone}}</td>
						  <td>
							 <button class="btn btn-danger" onClick="delEmployee({{$employee->id}})">Eliminar</button>
							  <a class="btn btn-primary text-white" href="{{asset('employee/'.$employee->id)}}">Ver</a>
						  </td>
						</tr>
						@endforeach
					  </tbody>
					</table>
					
					<div class="modal" id="confirm" tabindex="-1" role="dialog">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header bg-danger text-white">
							<h5 class="modal-title">¿Estás seguro?</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<p>Si le das al botón continuar el empleado será eliminado.</p>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-warning" data-dismiss="modal" id="deleteEmployee">Continuar</button>
						  </div>
						</div>
					  </div>
					</div>
					@else
						Actualmente no hay empleados creados.
					@endif
                </div>
				<div class="card-footer">
					{{ $employees->links() }}
				</div>
            </div>
        </div>
    </div>
	<div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Listado de compañías (DataTables)</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="dataTablesEmployee">
						  <thead>
							<tr class="bg-primary text-white">
							  <th scope="col">#</th>
							  <th scope="col">Nombre</th>
							  <th scope="col">Apellidos</th>
							  <th scope="col">Correo</th>
							</tr>
						  </thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script	src="{{asset('/js/administrador.js')}}" type="text/javascript"></script>
<script>
$(function () {
    $('#dataTablesEmployee').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			url:"{{asset('employee')}}"
		},
		columns:[
			{ data: 'id', name: 'id' },
        	{ data: 'names', name: 'names' },
        	{ data: 'lastnames', name: 'lastnames' },
        	{ data: 'email', name: 'email' },
		]
	});
});
</script>
@endsection
