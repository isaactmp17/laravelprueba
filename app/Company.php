<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'email'
    ];
    public function employee()
    {
        return $this->hasMany('App\Employee','companies_id');
    }
    public static function boot() {
        parent::boot();
        static::deleting(function($company) {
            $company->employee()->delete();
        });
    }
}
