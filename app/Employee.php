<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'names', 'lastnames', 'companies_id','email','phone',
    ];
    public function company()
    {
        return $this->belongsTo('App\Company','companies_id');
    }
}
