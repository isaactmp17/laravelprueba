<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Employee;
use App\Company;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return datatables()->of(Employee::select('id','names','lastnames','email')->get())->toJson();
        }else{
            $employees = Employee::with('company')->paginate(10);
            return view('employee.listemployees', ['employees' => $employees]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies=Company::all();
        return view('employee.createemployee', ['companies' => $companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nombres' => 'required|max:191',
            'apellidos' => 'required|max:191',
            'cid' => 'required|numeric',
            'correo' => 'required|email:rfc|max:191', // Para validar remotamente el correo utilizar email:rfc,dns. Lo desactivé porque necesita internet y si no tiene, puede arrojar error.
            'telefono' => 'required|numeric|digits_between:7,10',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if(Company::where('id',$request->cid)->exists()) {
            $employee = new Employee();
            $employee->names = $request->nombres;
            $employee->lastnames = $request->apellidos;
            $employee->companies_id = $request->cid;
            $employee->email = $request->correo;
            $employee->phone = $request->telefono;
            $employee->save();
            return response()->json(['success' => true]);
        }else{
            return response()->json(['errors'=>['Hubo un problema al realizar su solicitud, por favor refresque la página...']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Employee::where('id',$id)->exists()) {
            $employee=Employee::where('id',$id)->with('company')->first();
            return view('employee.showemployee')->with(['employee'=>$employee]);
        }else{
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Employee::where('id',$id)->exists()) {
            $employee=Employee::where('id',$id)->first();
            $companies=Company::all();
            return view('employee.editemployee')->with(['employee'=>$employee,'companies'=>$companies]);
        }else{
            abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Employee::where('id',$id)->exists()){
            $validator = \Validator::make($request->all(), [
                'nombres' => 'required|max:191',
                'apellidos' => 'required|max:191',
                'cid' => 'required|numeric',
                'correo' => 'required|email:rfc|max:191', // Para validar remotamente el correo utilizar email:rfc,dns. Lo desactivé porque necesita internet y si no tiene, puede arrojar error.
                'telefono' => 'required|numeric|digits_between:7,10',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            if(Company::where('id',$request->cid)->exists()) {
                $employee = Employee::where('id',$id)->first();
                $employee->names = $request->nombres;
                $employee->lastnames = $request->apellidos;
                $employee->companies_id = $request->cid;
                $employee->email = $request->correo;
                $employee->phone = $request->telefono;
                $employee->save();
                return response()->json(['success' => true]);
            }else{
                return response()->json(['errors'=>['Hubo un problema al realizar su solicitud, por favor refresque la página...']]);
            }
        }else{
            abort('404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Employee::where('id',$id)->exists()){
            $employee=Employee::where('id',$id)->first();
            $employee->delete();
            return response()->json(['success'=>true]); // Solo hago el borrado logico (sin borrar el logo)
        }else{
            return response()->json(['error'=>true]);
        }
    }
}
