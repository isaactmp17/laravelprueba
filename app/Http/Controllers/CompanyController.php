<?php

namespace App\Http\Controllers;

use App\Mail\EmpresaCreada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return datatables()->of(Company::select('id','name','email')->get())->toJson();
        }else {
            $companies = Company::paginate(10);
            return view('company.listcompanies', ['companies' => $companies]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.createcompany');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nombre' => 'required|max:191',
            'correo' => 'required|email:rfc|max:191', // Para validar remotamente el correo utilizar email:rfc,dns. Lo desactivé porque necesita internet y si no tiene, puede arrojar error.
            'logo' => 'required|image|mimes:png|max:2048|dimensions:min_width=150,min_height=150',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $company= new Company();
        $company->name=$request->nombre;
        $company->email=$request->correo;
        $company->save();

        $archivoSubido = $request->file('logo');
        $filename = $company->id.'.png';
        Storage::disk('public')->putFileAs(
            '/',
            $archivoSubido,
            $filename
        );

        // Mail::to('admin@admin.com')->send(new EmpresaCreada($company)); // Descomentar para activar el envío de correos.
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Company::where('id',$id)->exists()) {
            $company=Company::where('id',$id)->with('employee')->first();
            return view('company.showcompany')->with(['company'=>$company]);
        }else{
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Company::where('id',$id)->exists()) {
            $company=Company::where('id',$id)->first();
            return view('company.editcompany')->with(['company'=>$company]);
        }else{
            abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Company::where('id',$id)->exists()){
            $archivo=false;
            if ($request->hasFile('logo')) {

                $validator = \Validator::make($request->all(), [
                    'nombre' => 'required|max:191',
                    'correo' => 'required|email:rfc|max:191', // Para validar remotamente el correo utilizar email:rfc,dns. Lo desactivé porque necesita internet y si no tiene, puede arrojar error.
                    'logo' => 'required|image|mimes:png|max:2048|dimensions:min_width=150,min_height=150',
                ]);
                $archivo=true;
            }else{
                $validator = \Validator::make($request->all(), [
                    'nombre' => 'required|max:191',
                    'correo' => 'required|email:rfc|max:191', // Para validar remotamente el correo utilizar email:rfc,dns. Lo desactivé porque necesita internet y si no tiene, puede arrojar error.
                ]);
            }
            if (!$validator->fails()) {
                $company=Company::where('id',$id)->first();
                if($archivo){
                    if(Storage::disk('public')->exists($company->id.'.png')){
                        $archivoSubido = $request->file('logo');
                        $filename = $company->id.'.png';
                        Storage::disk('public')->putFileAs(
                            '/',
                            $archivoSubido,
                            $filename
                        );
                        $company->name=$request->nombre;
                        $company->email=$request->correo;
                        $company->save();
                    }
                }else{
                    $company->name=$request->nombre;
                    $company->email=$request->correo;
                    $company->save();
                }
                return response()->json(['success'=>true]);
            }
            return response()->json(['errors'=>$validator->errors()->all()]);
        }else{
            abort('404');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Company::where('id',$id)->exists()){
            $company=Company::where('id',$id)->first();
            $company->delete();
            return response()->json(['success'=>true]); // Solo hago el borrado logico (sin borrar el logo)
        }else{
            return response()->json(['error'=>true]);
        }

    }
}
